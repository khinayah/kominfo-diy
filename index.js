const dotenv = require('dotenv')
const express = require("express")
var cors = require('cors')

const router = require("./src/routes/main")

const app = express()
const port = 8080
app.use(cors())

dotenv.config()
app.use(express.json())
app.use("/", router)

// var corsOptions = {
//     origin: 'http://localhost:5173/', // ini merupakan host frontend yang nantinya akan menggunakan backend ini
//     optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204n
//     credentials: true
//   }

// app.use(cors(corsOptions))



app.listen(port, () => {
    console.log(`app listening on port ${port}`)
})