const express = require('express')
const { register, login, changePassword, deleteUser, getAllUser } = require('../controllers/authController')
const { getCourses, createCourse, deleteCourse, updateCourse } = require('../controllers/courseController')
const { jwtAuth, jwtAuthAdmin } = require('../middleware/authentication')
const { createUser } = require('../controllers/userController')
const { getUserCourses, getBachelorTitle, getAnotherBachelorTitle, getCourseParticipants } = require('../controllers/userCourseController')

const router = express.Router()

router.get('/', (req, res) => {
    res.send("Welcome to Kominfo API")
})

router.post('/register', register)
router.post('/login', login)
router.patch('/change-password', changePassword)
router.get('/user', jwtAuthAdmin, getAllUser)
router.delete('/user/:id', jwtAuthAdmin, deleteUser)

router.get('/user-courses', jwtAuth, getUserCourses)
router.get('/user-courses/sarjana', jwtAuth, getBachelorTitle)
router.get('/user-courses/bukan-sarjana', jwtAuth, getAnotherBachelorTitle)
router.get('/user-courses/course-participants', jwtAuth, getCourseParticipants)


router.post('/user', jwtAuth, createUser)

router.get('/courses', jwtAuth, getCourses)
router.post('/courses', jwtAuth, createCourse)
router.patch('/courses/:id', jwtAuth, updateCourse)
router.delete('/courses/:id', jwtAuth, deleteCourse)


module.exports = router