const {PrismaClient} = require ('@prisma/client')
const { json } = require('express')

const prisma = new PrismaClient()


const getUserCourses = async (req, res) => {
        const userCourses = await prisma.userCourses.findMany({
          select: {
            id: true,
            user: {
              select: {
                id: true,
                username: true,
              },
            },
            course: {
              select: {
                course: true,
                mentor: true,
                title: true,
              }
            }
          }
        })
    
        return res.status(200).json(userCourses)
}

const getBachelorTitle = async (req, res) => {
    const userCourses = await prisma.userCourses.findMany({
    where: {
      course: {
        title: {
          startsWith: 'S'
        }
      }
    },
    select: {
      id: true,
      user: {
        select: {
          id: true,
          username: true
        }
      },
      course: {
        select: {
          course: true,
          mentor: true,
          title: true
        }
      }
    }
  })
  return res.status(200).json(userCourses)
}

const getAnotherBachelorTitle = async (req, res) => {
    const userCourses = await prisma.userCourses.findMany({
        where: {
            NOT: {
              course: {
                title: {
                  startsWith: 'S'
                }
              }
            }
          },
    select: {
      id: true,
      user: {
        select: {
          id: true,
          username: true
        }
      },
      course: {
        select: {
          course: true,
          mentor: true,
          title: true
        }
      }
    }
  })
  return res.status(200).json(userCourses)
}

const getCourseParticipants = async (req, res) => {
    try {
      const courseParticipants = await prisma.userCourses.groupBy({
        by: ['courseId', { Course: { mentor: true } }],
      _count: {
        courseId: true,
      }
    });
  
    return courseParticipants.map(({ courseId, _count, Course }) => ({
        course: courseId,
        mentor: Course.mentor,
        participant_count: _count,
      }));
    } catch (error) {
      console.error('Error fetching course participants:', error);
      throw error;
    }
  }
  

module.exports = {getUserCourses, getBachelorTitle, getAnotherBachelorTitle, getCourseParticipants}