const {PrismaClient} = require ('@prisma/client')
const { json } = require('express')

const prisma = new PrismaClient()


const getCourses = async (req, res) => {
    let course = await prisma.courses.findMany({
        orderBy: [ 
            { 
                createdAt: 'desc', 
            }, 
        ]
    })

    return res.status(200).json(course)
}

const getCoursesById = async (req, res) => {
    let {id} = req.params
    id = parseInt(id)

    try{
        const course = await prisma.courses.findUnique({where: {id}})

        return course ? res.json(course) : res.status(404).json({ message: `course with id ${id} doesn't exist` })
    }catch(err){
        return res.status(404).json({error:err})
    }
}

const createCourse = async (req, res) => {
    let {course, mentor, title} = req.body

    try {
        const pelajaran = await prisma.courses.create({data: {course, mentor, title}})
        return res.status(200).json(pelajaran)
    }catch(err) {
        return res.status(404).json({error: err})
    }
}

const updateCourse = async (req, res) => {
    let { id } = req.params;
    id = parseInt(id);
    let { course, mentor, title } = req.body;

    try {
        const existingCourse = await prisma.courses.findUnique({ where: { id } });

        if (!existingCourse)
            return res.status(404).json({ error: `Course with id ${id} doesn't exist` });

        const updatedCourse = await prisma.courses.update({
            where: { id },
            data: {
                course: course || existingCourse.course,
                mentor: mentor || existingCourse.mentor,
                title: title || existingCourse.title,
                updatedAt: new Date()
            }
        })

        return res.status(200).json(updatedCourse);
    } catch (err) {
        return res.status(404).json({error: err})
        }
}


const deleteCourse = async (req, res) => {
    let {id} = req. params
    id = parseInt(id)

    try {
        const course = await prisma.courses.delete({
            where: {id}
        })
        return res.status(200).json("course was successfully deleted")
        }catch (err) {
        return res.status(404).json({error: err})
        }
}


module.exports = {
    getCourses,
    createCourse,
    updateCourse,
    deleteCourse}