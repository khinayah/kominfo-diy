const { PrismaClient } = require('@prisma/client')
const bcrypt = require("bcryptjs")
const jwt = require('jsonwebtoken')
const { getUser } = require('../utils/helper')

const prisma = new PrismaClient()


const register = async (req, res) => {
  let { email, username, role, password } = req.body

  const hashedPassword = await bcrypt.hash(password, 10)

  const existingEmail = await prisma.user.findUnique({
    where: {
      email
    }
  })

  if (existingEmail)
    return res.status(400).json({ message: `User with email ${email} already exist` })

  const existingUsername = await prisma.user.findUnique({
    where: {
      username
    }
  })

  if (existingUsername)
    return res.status(400).json({ message: `User with username ${username} already exist` })

  const user = await prisma.user.create({ data: { email, username, role: "user", password: hashedPassword } })

  const expiresIn = process.env.EXPIRE_HOURLY * 3600

  const token = jwt.sign({ userId: user.id, role: user.role }, process.env.TOKEN_SECRET, { expiresIn })


  return res.status(200).json({ user: { id: user.id, email, username, role }, token })
}

const login = async (req, res) => {
  let { email, username, password } = req.body

  const users = await prisma.user.findMany({
    where: {
      OR: [
        { username },
        { email }
      ]
    }
  })

  if (users.length > 0) {
    let user = users[0]
    const valid = await bcrypt.compare(password, user.password)
    if (!valid) {
      return res.status(400).json("invalid credentials")
    } else {
      const expiresIn = process.env.EXPIRE_HOURLY * 3600

      const token = jwt.sign({ userId: user.id, role: user.role }, process.env.TOKEN_SECRET, { expiresIn });
      const { id, email, username, role } = user


      return res.status(200).json({ user: { id, username, email, role }, token })
    }
  } else {
    return res.status(400).json("invalid credentials")
  }
}


const changePassword = async (req, res) => {
  let { currentPassword, newPassword, confirmNewPassword } = req.body
  let { userId } = getUser(req)

  const users = await prisma.user.findMany({
    where: {
      id: userId
    }
  })

  if (users.length > 0) {
    let user = users[0]
    const valid = await bcrypt.compare(currentPassword, user.password)
    const inputPassword = newPassword === confirmNewPassword ? true : false
    if (valid) {
      if (inputPassword) {
        const hashedPassword = await bcrypt.hash(newPassword, 10)
        const changePassw = await prisma.user.update({
          data: {
            email: user.email,
            password: hashedPassword
          },
          where: {
            id: userId
          }
        })
        return res.status(200).json(changePassw)
      } else {
        return res.status(400).json("New Password and Confirm New Password doesn't match!")
      }
    } else {
      return res.status(400).json("Current Password doesn't match")
    }
  } else {
    return res.status(400).json("invalid credential")
  }
}

const getAllUser = async (req, res) => {
  const user = await prisma.user.findMany({
    where : {
      role: {
        equals: 'user',
      },
    },
  })

  return res.status(200).json(user)
}

const deleteUser = async (req, res) => {
  let { id } = req.params
  id = parseInt(id)

  try {
    const user = await prisma.user.delete({
      where: { id }
    })
    return res.status(200).json("user was successfully deleted")
  } catch (err) {
    return res.status(404).json({ error: err })
  }
}


module.exports = {
  register, login,
  changePassword,
  getAllUser,
  deleteUser
}