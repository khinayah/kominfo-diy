const {PrismaClient} = require ('@prisma/client')
const { json } = require('express')

const prisma = new PrismaClient()


const getUser = async (req, res) => {
    let user = await prisma.user.findMany({
        orderBy: [ 
            { 
                createdAt: 'desc', 
            }, 
        ]
    })

    return res.status(200).json(user)
}


const getUserById = async (req, res) => {
    let {id} = req.params
    id = parseInt(id)

    try{
        const user = await prisma.user.findUnique({where: {id}})

        return user ? res.json(user) : res.status(404).json({ message: `user with id ${id} doesn't exist` })
    }catch(err){
        return res.status(404).json({error: err })
    }
}


const createUser = async (req, res) => {
    let {username} = req.body

    const existingUser = await prisma.user.findUnique({ 
        where: {
          username
        }})
    
      if(existingUser)
      return res.status(400).json({ message : `user ${username} already exist`})

    try {
        const user = await prisma.user.create({data: {user}})
        return res.status(200).json(user)
    }catch(err) {
        return res.status(404).json({error: err})
    }
}


// const updateCategory = async (req, res) => {
//     let {id} = req.params
//     id = parseInt(id)
//     let {name} = req.body

//     const category = await prisma.Category.findUnique({where: {id}})

//     if (category === null)
//         return res.status(404).json({ message: `Category with id ${id} doesn't exist` })


//     const existingCategory = await prisma.Category.findUnique({ 
//         where: {
//           name
//         }})
    
//     if (existingCategory)
//     return res.status(400).json({ message : `Category ${name} already exist`})

//     try {
//         const category = await prisma.category.update({
//             where: {id},
//             data: {
//                 name,
//                 updatedAt: new Date()
//             },
//         })
//         return res.status(200).json(category)
//     }catch(err) {
//         return res.status(404).json({error: err})
//     }
// }


// const deleteUser = async (req, res) => {
//     let {id} = req. params
//     id = parseInt(id)

//     try {
//         const category = await prisma.category.delete({
//             where: {id}
//         })
//         return res.status(200).json("category was successfully deleted")
//         }catch (err) {
//         return res.status(404).json({error: err})
//         }
// }


module.exports = {getUser, createUser}